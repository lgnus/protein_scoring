"""
All utilities pertaining to the editing and management of pdb files
"""

__author__ = 'lgnus'

import os
import re
import sys
import shutil
import mdtraj as md

from pathlib import Path
import pyrosetta as pr

import config as cfg


def rename_to_chain(pdb_contents, chain_name):
    """ Renames the chains in the pdb provided with <chain_name>

    Parameters
    ----------
        pdb_contents : string 
            contents of the pdb file
        chain_name : char
            new chain name
    Returns
    -------
        new_contents : string
            contents of the pdb file after chain name modification
    """

    chain_pattern = re.compile(r'(?<=\w{3}\s)[A-Z](?=\s+\d+)')
    new_contents, _ = re.subn(chain_pattern, chain_name, pdb_contents)
    return new_contents


def split_dockground_decoys(pdb_codes, output_path='dockground_split', receptor_copy=True, rename_chains=True):
    """ Loads decoys from dockground for all the <pdb_codes> provided and splits each decoy into a
    different pdb file at <output_path>

    Parameters
    ----------
        pdb_codes : array-like
            list of pdb codes from dockground dataset to split
        output_path : pathlib.Path
            path to save the resulting split files
        receptor_copy : bool
            whether or not to also copy the receptor into the output folder
        rename_chains : bool
            whether or not the chains should be renamed (R for receptor and L for ligand) 
    """

    for code in pdb_codes:

        # create the output folder for a given pdb
        path = f'{str(output_path)}/{code}/'
        os.makedirs(path, exist_ok=True)

        # if receptor should be copied
        if receptor_copy:
            shutil.copyfile(cfg.dockground_receptor_path(code), Path(path + 'receptor.pdb'))

            # if the chain should be renamed
            if rename_chains:
                with open(Path(path + 'receptor.pdb'), 'r+') as receptor:
                    contents = rename_to_chain(receptor.read(), 'R')
                    receptor.seek(0)
                    receptor.truncate()
                    receptor.writelines('\n'.join(contents.splitlines()[:-1]))
        
        # open the decoys file and obtain the models
        with open(cfg.dockground_ligand_path(code)) as lig_file:
            models = lig_file.read().split('ENDMDL\n')
            del models[-1]        # delete END split

        # iterate the models and save them into their own
        # individual files
        for idx, model in enumerate(models):
            with open(Path(path + f'lig.{idx}.pdb'), 'w') as ligand:
                contents = '\n'.join(model.splitlines()[1:])

                # rename the chain if option is set to true
                if rename_chains:
                    contents = rename_to_chain(contents, 'L')
                
                ligand.write(contents)


def split_dockground_and_complex(pdb_codes, output_path='dockground_split_complexed', rename_chains=True):
    """ Loads decoys from dockground for all the <pdb_codes> provided and splits each decoy into the complex
    obtained from merging the decoys with a copy of the receptor and saves them into different pdb files at
    <output_path>

    Parameters
    ----------
        pdb_codes : array-like
            list of pdb codes from dockground dataset to split
        output_path : pathlib.Path
            path to save the resulting split files
        rename_chains : bool
            whether or not the chains should be renamed (R for receptor and L for ligand) 
    """
    # first call the split to make sure they exist, then create the complexes from them.
    split_dockground_decoys(pdb_codes, rename_chains=rename_chains)

    for code in pdb_codes:

        receptor = md.load(f'dockground_split/{code}/receptor.pdb')
        os.makedirs(f'{output_path}/{code}', exist_ok=True)

        for idx in range(100):
            ligand = md.load(f'dockground_split/{code}/lig.{idx}.pdb')
            complex_ = receptor.stack(ligand)
            complex_.save_pdb(f'{output_path}/{code}/complex.{idx}.pdb')


def create_complex(receptor, ligand):
    """ Given two molecules, creates a single pose containing both.

    Parameters
    ----------
        receptor : pyrosetta.Pose
            receptor pose of the complex
        ligand : pyrosetta.Pose

    Returns
    -------
        complex_ : pyrosetta.Pose
            Complex formed by the junction of the two provided poses.
    """

    complex_ = pr.Pose() 
    pr.rosetta.core.pose.append_pose_to_pose(complex_, receptor)
    pr.rosetta.core.pose.append_pose_to_pose(complex_, ligand)

    return complex_


def load_pdb(filepath, multiple=False):
    """ Loads a single pdb file from <filepath> and returns it. If <multiple> is set to True
    instead return an array containing all the poses inside the pdb file.

    Parameters
    ----------
        filepath : str
            path to the pdb file
        multiple : bool
            whether or not the pdb contains multiple poses

    Returns
    -------
        poses : pyrosetta.Pose / numpy.array(pyrosetta.Pose)
            returns the pyrosetta pose extracted from the provided pdb, or an array with all
            the poses in it, if multiple is set to true. 
    """
    if multiple:
        poses = pr.rosetta.utility.vector1_core_pose_Pose()
        pr.pose_from_file(poses, str(filepath))
    else:
        poses = pr.pose_from_pdb(str(filepath))

    return poses