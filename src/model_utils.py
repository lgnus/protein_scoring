import sys
import numpy as np
import pandas as pd

import pickle
from tqdm import tqdm
from sklearn.feature_selection import VarianceThreshold

sys.path.append('..')
import config as cfg


def dcg_score(y_true, y_score, k=10, gains="exponential"):
    """Discounted cumulative gain (DCG) at rank k
    Parameters
    ----------
    y_true : array-like, shape = [n_samples]
        Ground truth (true relevance labels).
    y_score : array-like, shape = [n_samples]
        Predicted scores.
    k : int
        Rank.
    gains : str
        Whether gains should be "exponential" (default) or "linear".
    Returns
    -------
    DCG @k : float
    """
    order = np.argsort(y_score)[::-1]
    y_true = np.take(y_true, order[:k])

    if gains == "exponential":
        gains = 2 ** y_true - 1
    elif gains == "linear":
        gains = y_true
    else:
        raise ValueError("Invalid gains option.")

    # highest rank is 1 so +2 instead of +1
    discounts = np.log2(np.arange(len(y_true)) + 2)
    return np.sum(gains / discounts)


def ndcg_score(y_true, y_score, k=10, gains="exponential"):
    """Normalized discounted cumulative gain (NDCG) at rank k
    Parameters
    ----------
    y_true : array-like, shape = [n_samples]
        Ground truth (true relevance labels).
    y_score : array-like, shape = [n_samples]
        Predicted scores.
    k : int
        Rank.
    gains : str
        Whether gains should be "exponential" (default) or "linear".
    Returns
    -------
    NDCG @k : float
    """
    best = dcg_score(y_true, y_true, k, gains)
    actual = dcg_score(y_true, y_score, k, gains)
    return actual / best

def load_model(filename):
    """
    Loads a previously saved machine learning model.

    Parameters
    ----------
    filename : str
        name of the saved model to load
    
    Returns
    -------
    model : machine_learning model
        previously saved model with provided <filename>
    """
    return pickle.load(open(f'data/tmp/{filename}', 'rb'))


def save_model(model, filename):
    """
    Saves a machine learning model into a pickle

    Parameters
    ----------
    model : machine_learning model
        trained model
    filename : str
        filename with which to save the model
    """
    pickle.dump(model, open(f'data/tmp/{filename}', 'wb'))


def save_model_score(score, y_test, filename, ascending=True):
    """
    Saves the scores of a classification/regression models

    Parameters
    ----------
    score : array-like 
        score of the model
    y_test : pandas.DataFrame
        a dataframe with the true labels and pdb_codes of each entry
    filename: string
        name of the file to save with
    ascending : boolean
        Whether or not the score is better if lower
    """
    res = pd.DataFrame()
    res['score'] = score if ascending else 1 - score
    res['pdb_code'] = np.array(y_test.pdb_code)
    res['native'] = np.array(y_test.native)
    res['idx'] = np.array(y_test.idx)
    res['rmsds'] = np.array(y_test.rmsds)

    res.to_pickle(f"data/tmp/{filename}")


def save_improved_score(other_score, model_score, filename, threshold=0.4):
    improved_preds = pd.DataFrame()
    for code in other_score.pdb_code.unique():
        df = other_score.loc[other_score['pdb_code'] == code].copy()
        dis = model_score.loc[model_score.pdb_code == code].sort_values('idx')
        
        df['discard'] = np.array(dis['score'] > 1 - threshold)
        df.sort_values(['discard', 'score'], inplace=True)
        df['score'] = np.arange(100)
        
        improved_preds = pd.concat([improved_preds, df])

    improved_preds['idx'] = other_score.index    
    
    save_model_score(improved_preds.score, improved_preds, filename)


def get_hit_stats(models, sort_ascending=True, figsize=(15, 8),score_col='score'):

    model_hits = pd.DataFrame()
    for model_name, y in models.items():

        # get the scores from each model and sort the complexes by score
        results = y[['pdb_code', 'native', score_col]].copy()
        results.sort_values(score_col, inplace=True, ascending=sort_ascending)

        # get the predicted ranks for each pdb in the set
        hits = []
        for pdb in y.pdb_code.unique():
            temp = results.loc[results.pdb_code == pdb].copy()
            temp['rank'] = list(range(1, 101))
            hits.append(int(temp.loc[temp.native == True, 'rank']))
            
            # model_hits[model_name] = hits
            
        model_hits = pd.concat([model_hits, pd.DataFrame(hits)], axis=1, ignore_index=True)
    model_hits.columns = models.keys()

    return model_hits