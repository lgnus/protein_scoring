"""
Configuration file for the project.
"""

__author__ = 'lgnus'

from enum import Enum
from pathlib import Path
# import pyrosetta.rosetta as pr_r

# =============== [GENERAL]
RANDOM_SEED = 42
LOG_PATH = 'extraction.log'
SCORES_PATH = 'data/scores/'
ML_MODEL_PATH = 'data/ml_models'

# =============== [PATHS]
FASTCONTACT_PATH = Path('benchmarking/fastcontact')
DDFIRE_PATH = Path('benchmarking/ddfire')
SPIDER_PATH = Path('benchmarking/spider')
DOCKGROUND_PATH = Path('/home/lgnus/Projects/protein_scoring/data/dockground/')

dockground_receptor_path = lambda code: DOCKGROUND_PATH / f'{code}/{code}_u1.pdb'
dockground_ligand_path = lambda code: DOCKGROUND_PATH / f'{code}/{code}_u2_decoys.pdb'

# =============== [PERFORMANCE]
N_PROCESSES = 4     # FIXME currently unused as multiprocessing is not yet implemented

# =============== [PYROSETTA PARAMETERS]
ROSETTA_INIT = ' '.join([
    '-packing:no_optH True',
    '-ignore_unrecognized_res True',
    '-packing:pack_missing_sidechains False',
    '-empty True',
    '-rigid True',
    '-mute all'
])

# Features to be extracted on the separated receptor and ligand
PRE_COMPLEX_FEATS = [
    # pr_r.core.scoring.fa_sol,
    # pr_r.core.scoring.fa_elec,
    # pr_r.core.scoring.fa_sasa,
]

# Features to be extracted for each complex
COMPLEX_FEATS = [
    # pr_r.core.scoring.fa_atr,
    # pr_r.core.scoring.fa_rep,
    # #pr_r.core.scoring.fa_dun,
    # pr_r.core.scoring.fa_elec,
    # pr_r.core.scoring.fa_pair,
    # pr_r.core.scoring.fa_sol,
    # #pr_r.core.scoring.p_aa_pp,
    # pr_r.core.scoring.fa_sasa,
    # pr_r.core.scoring.interface_dd_pair,
    #pr_r.core.scoring.pack_stat,
    #pr_r.core.scoring.hbond_sr_bb,
    #pr_r.core.scoring.hbond_lr_bb,
    #pr_r.core.scoring.hbond_bb_sc,
    #pr_r.core.scoring.hbond_sc,
]

# =============== [BIO PARAMETERS]
INTERFACE_CUTOFF = 10       # Distance threshold for considering a residue part of the interface
CONTACT_CUTOFF = 7      # Distance threshold for considering a residue pair in contact (was 6 originally) 

# Residue classification mapping
residue_class = {
    'ALA': 'Hydrophobic',
    'ARG': 'Positive', 
    'ASN': 'Polar',
    'ASP': 'Negative',
    'CYS': 'Polar',
    'GLU': 'Negative',
    'GLN': 'Polar',
    'GLY': 'Hydrophobic',
    'HIS': 'Positive',
    'ILE': 'Hydrophobic',
    'LEU': 'Hydrophobic',
    'LYS': 'Positive',
    'MET': 'Hydrophobic',
    'PHE': 'Hydrophobic',
    'PRO': 'Hydrophobic',
    'SER': 'Polar',
    'THR': 'Polar',
    'TRP': 'Hydrophobic',
    'TYR': 'Polar',
    'VAL': 'Hydrophobic',
}    

# =============== [CAPRI CRITERIA]
CAPRI_LRMSD = 10
CAPRI_IRMSD = 4
CAPRI_FNC = 0.1
