""" 
This script exists to generate decoys from ZDOCK datasets.
"""

import sys
import os, re
import shutil
from pathlib import Path

import begin
from tqdm import tqdm

@begin.start
@begin.convert(zdock_root=str, pdb_codes=list, n_complexes=int, output_folder=str)
def main(zdock_root='../data/decoys_bm4_zd3.0.2_irad', pdb_codes=None, n_complexes=2000, output_folder='output'):

    # create paths to zdock's different files / folders 
    # according to zdocks' usual folder structure
    inputs_path = zdock_root / "input_pdbs"
    outputs_path = zdock_root / "results"

    # paths to the two other dependencies
    dep_1_path = zdock_root / "create.pl"
    dep_2_path = zdock_root / "create_lig"

    # get files list in input path
    input_files = [file for file in inputs_path.glob('*')]

    # get list of available pdb codes 
    if pdb_codes == None:
        pdb_codes = set([filename.split('_')[0].lower() for filename in input_files])
    
    # Iterate over all the pdb codes, copy the files necessary for
    # generation into the folder, create the complexes and clean up
    # the non-complex files
    sout = tqdm(pdb_codes, total=len(pdb_codes))
    for pdb_code in sout:
        sout.write(f'Handling {pdb_code}...')

        # create folders
        cur_path = Path(f'{output_folder}/{pdb_code}')
        os.makedirs(cur_path, exist_ok=True)

        # copy dependencies
        shutil.copy(dep_1_path, cur_path)
        shutil.copy(dep_2_path, cur_path)

        # TODO  check if better way
        pattern = re.compile(f'^{pdb_code}.*')
        for filepath in [f for f in inputs_path.glob('*') if re.match(pattern, f.name) != None]:
            shutil.copy(filepath, cur_path)
        for filepath in [f for f in outputs_path.glob('*.irad.out') if re.match(pattern, f.name) != None]:
            shutil.copy(filepath, cur_path)

        # generate complexes
        os.system(f'cd {cur_path} && ./create.pl {pdb_code}.zd3.0.2.irad.out {n_complexes}')

        # remove files that are no longer needed
        for filepath in [f for f in cur_path.glob('*') if re.match(re.compile(r'^complex.*'), f.name) == None]:
            os.remove(filepath)

