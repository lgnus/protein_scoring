## Protein Scoring

Protein scoring function using pyrosetta and machine learning techniques.

Note: Some of the code, namely feature extraction related, got slight changes to reflect the final features used, this means some features got incorporated straight into the scoring function and some removed as they were deemed non-informative. The notebooks suffered many changes throughout time, and may not reflect everything that was done, in such cases a comment usually explains what happened.

---

#### Structure
* `config.py`: Configuration file, allows selection of which features to extract, classification of protein amino acids, criteria for interfaces / contacts and other parameters of the sorts.
* src/
  * `feature_extraction.py` : script to extract features from the dockground data.
  * `model_utils.py` : utilities used during the model training stage, save/load models, custom score function, etc.
  * `plot_utils.py` : useful plotting functions used throughout the work to generate graphics for inspection.
  * `pdb_utils.py` : methods for manipulation of pdb files.
  * `zdock_generate.py` : script to generate putative complexes for the zdock dataset.
  * `borutapy_imps.py`: edited implementation of [BorutaPy](https://github.com/scikit-learn-contrib/boruta_py) that exposes feature importances.
* data/
  * `dockground/` : dockground dataset, 
  * `others/`: other data sources such as ProteinDataBank data, used for creating some graphs.
  * `times/` : data pertaining to the time cost of extracting scores for the dockground test subset.
  *  `cv_splits.npy` : cross-validation splits, saved for reuse
  *  `raw_features` : raw features as calculated from the feature extraction process.
  *  `x_test` : unlabeled data for the test set
  *  `x_train`: unlabeled data for the training set
  *  `y_test` : labels for the test set
  *  `y_train`: labels for the training set
* benchmarking/
  * `ddfire/` : implementation of dDFIRE used, downloaded from [here](http://sparks-lab.org/pmwiki/download/index.php?Download=yueyang/DFIRE2-pair-src2.tgz)
  * `fastcontact/`: implementation of FastContact used, downloaded from [here](http://structure.pitt.edu/software/FastContact/)
  * `spider/`: implementation of SPIDER used, downloaded from [here](http://www.bioinformatics.org/spider/#Download)
  * `dDFIRE.ipynb` : notebook that manages the necessary files and running of the dDFIRE scoring function.
  * `FastContact.ipynb` : notebook that manages the necessary files and running of the FastContact scoring function.
  * `SPIDER.ipynb` : notebook that manages the necessary files and running of the SPIDER scoring function.
* `EDA_FeatureSelection.ipynb` : Basic exploration of the available features and feature selection process.
* `Model_Training.ipynb` : Creation of the main models.
* `Performance_Assessment.ipynb` :  Notebook to generate several performance assessment graphics.

---

#### Dependencies


* `pyrosetta` : software used to parse the pdb files and extract several features for each complex, can be downloaded from [here](http://www.pyrosetta.org/dow), requires a license which can be requested for non-commercial purposes.

* `begins` : python package to handle command line arguments, installed with `pip install begins`
* `mdtraj` : package used for a small manipulation necessary to run one of the scoring functions, local code can be made to replace it so as to no longer require this dependency, but for the time being it is needed.
* `libg2c0` is required to run FastContact, installed on -buntu with the following code: `sudo dpkg -i --force-all libg2c0_3.4.6-6ubuntu5_i386.deb` 