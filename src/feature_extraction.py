"""
This script handles everything pertaining to the process of feature extraction.
"""

import sys
import time
import logging

import begin
import pandas as pd
import pyrosetta as pr
import numpy as np
from pathlib import Path
from tqdm import tqdm

sys.path.append('..')
import config as cfg
import src.pdb_utils as pu


class FeatureExtractor():

    def __init__(self):
        
        # initialize pyrosetta with parameterization defined in config
        pr.init(cfg.ROSETTA_INIT)

        # prepare pre-complex score function     
        self.pre_scorefx = pr.ScoreFunction()
        [self.pre_scorefx.set_weight(feature, 1) for feature in cfg.PRE_COMPLEX_FEATS]

        # prepare complex score function
        self.scorefx = pr.ScoreFunction()
        [self.scorefx.set_weight(feature, 1.0) for feature in cfg.COMPLEX_FEATS] 

        # get list of all types of contacts
        self.contact_types = set(['-'.join(sorted([str(i), str(j)])) for i in cfg.residue_class.values() for j in cfg.residue_class.values()])

    def get_precomplex_features(self, receptor, ligand):
        """ Applies rosetta scoring function with the features defined in the config file to
        both receptor and ligand and returns the feature scores and their sum in a feature map.

        Parameters
        ----------
            receptor : pyrosetta.Pose
                receptor molecule from which to extract the features.
            ligand : pyrosetta.Pose
                ligand molecule from which to extract the features.
        
        Returns
        -------
            pre_feats : Dict
                dicionary containing the name of the feature and its corresponding value
                as calculated with pyrosetta score function.
        """
            # apply the score function to the receptor and a ligand
        self.pre_scorefx(receptor)
        self.pre_scorefx(ligand)

        rec_energies = receptor.energies().total_energies() 
        lig_energies = ligand.energies().total_energies()

        # create a dict with the features and corresponding names
        pre_feats = {str('pre_' + str(feature).split(".", 1)[1]): lig_energies[feature] + rec_energies[feature] for feature in cfg.PRE_COMPLEX_FEATS}

        # NOTE: These features are not used in the final version, uncomment to add them back
        #pre_feats.update(({str('rec_' + str(feature).split(".", 1)[1]): rec_energies[feature] for feature in cfg.PRE_COMPLEX_FEATS}))
        #pre_feats.update(({str('lig_' + str(feature).split(".", 1)[1]): lig_energies[feature] for feature in cfg.PRE_COMPLEX_FEATS}))
        #pre_feats.update({'rec_size': receptor.size(), 'lig_size': ligand.size()})

        return pre_feats

    def get_pyrosetta_features(self, complex_):
        """ Applies rosetta scoring function with the features defined in the config file to
        a complex generated by merging a receptor molecule and a ligand molecule, and returns the
        feature scores in a feature map.
        
        Parameters
        ----------
            complex_ : pyrosetta.Pose
                molecule from which to extract the features.
        
        Returns
        -------
            feats : Dict
                dicionary containing the name of the feature and its corresponding value
                as calculated with pyrosetta score function.
        """
        self.scorefx(complex_)

        # create dict with the extracted energies
        energies = complex_.energies().total_energies()
        complex_feats = {str(feature).split(".", 1)[1]: energies[feature] for feature in cfg.COMPLEX_FEATS}
        
        return complex_feats

    def get_interfacial_features(self, complex_):
        """ Extracts additional features from the interfaces and pairwise contacts between
        the two molecules comprising the complex. 

        Parameters
        ----------
            complex_ : pyrosetta.Pose
                pose of the complex
        
        Results
        -------
            features : Dict
                dictionary containing all the interfacial and pairwise contact features.
        """

        # Obtain interfacial residues. "A_B" is the default chain names when creating a complex with pyrosetta
        interface_idx = pr.rosetta.protocols.interface.select_interface_residues(complex_, 'A_B', cfg.INTERFACE_CUTOFF)
        interface_res = {idx: complex_.residue_type(idx).base_name()[:3] for idx in range(1, complex_.size() + 1) if interface_idx[idx] == True}

        # Get the interfacial residues from each molecule 
        rec_end = complex_.chain_end(1)     # end of the receptor molecule   
        rec_inter = [res for res in range(1, rec_end + 1) if interface_idx[res] == True]
        lig_inter = [res for res in range(rec_end + 1, complex_.size() + 1) if interface_idx[res] == True]

        # create a dict to store the total counts for each contact type and another for other features
        contact_counter = dict(zip(self.contact_types, np.zeros(len(self.contact_types))))
        interface_feats = {
            'inter_size': len(interface_res),
            'inter_ratio': len(interface_res) / complex_.size(),
            'inter_hydro': len([res for res in interface_res.values() if cfg.residue_class[res] == 'Hydrophobic']) / len(interface_res),
        }

        # iterate over each interfacial residue from one molecule, find and count the contacts
        # on the other molecule and extract their types.
        for rec_res in rec_inter:
            for lig_res in lig_inter:
                
                # get the coords for the CA and calculate the distance between them
                ca_r = np.array(complex_.residue(rec_res).xyz('CA'))
                ca_l = np.array(complex_.residue(lig_res).xyz('CA'))
                distance = np.sqrt(sum(ca_r - ca_l)**2)

                # if distance is below the defined cutoff add to the contact counter
                if distance <= cfg.CONTACT_CUTOFF:
                    rec_res_class = cfg.residue_class[complex_.residue_type(rec_res).base_name()[:3]]
                    lig_res_class = cfg.residue_class[complex_.residue_type(lig_res).base_name()[:3]]

                    contact_counter['-'.join(sorted([rec_res_class, lig_res_class]))] += 1

        return {**interface_feats, **contact_counter, 'ncontacts': sum(contact_counter.values())}

    def extract_seq(self, pdb_code): # TODO, update for other sources or create compat layer
        """ Given a pdb code, loads the respective files for receptor and ligands, then
        sequentially extracts the features for all the putative poses formed by their
        merging.

        Parameters
        ----------
            pdb_code : String
                pdb id code of the complex 

        Results
        -------
            feats : Dict
                dicionary containing the name of the feature and its corresponding value.
        """
        # load the receptor and ligand list for the specified pdb_code
        receptor = pu.load_pdb(cfg.dockground_receptor_path(pdb_code))
        ligand_list = pu.load_pdb(cfg.dockground_ligand_path(pdb_code), multiple=True) 

        # calculate the pre-complex features, any ligand would be fine, so the first
        # is chosen (values should be the same regardless)
        pre_feats = self.get_precomplex_features(receptor, ligand_list[1])

        # iterate over each decoy, merge and extract their features
        scores = []
        for idx, ligand in enumerate(ligand_list):

            # create complex
            complex_ = pu.create_complex(receptor, ligand)
            
            # calculate all features
            rosetta_feats = self.get_pyrosetta_features(complex_)
            inter_feats = self.get_interfacial_features(complex_)

            # get the dif between the pre-complex feats and the merged ones
            dif_feats = {f'{pre_name[4:]}_dif' : pre_feats[pre_name] - rosetta_feats[f'{pre_name[4:]}'] for (pre_name, pre_value) in pre_feats.items()}

            # merge all the calculated features and append them to the dict
            scores.append({'idx': idx, **dif_feats, **rosetta_feats, **inter_feats})

        return pd.DataFrame(scores)

# = = = = = = = = = = = = = = = = = XXX

@begin.start
def main(path=cfg.DOCKGROUND_PATH):
    """ 
    Extracts the features for all the pdb complexes within a folder, dockground folder structure
    is expected.
    """
    # save start time and prepare logs
    logging.basicConfig(filename=cfg.LOG_PATH, level=logging.DEBUG)
    extraction_start = time.perf_counter()

    # create the feature extractor object
    fe = FeatureExtractor()

    # extract the features for every pdb in the list of pdbs
    res = pd.DataFrame()

    # obtain the pdb files from the specified folder
    pdbs = [f.name for f in cfg.DOCKGROUND_PATH.iterdir()]

    for code in pdbs:
        try:
            # extract the features and add information about the complex
            complex_feats = fe.extract_seq(code)
            complex_feats['pdb_code'] = code

            # append the results to a dataframe containing all of them
            res = pd.concat([res, complex_feats])

        except:     # If it fails to extract for a given complex, proceed regardless
            continue 

    # save the results to a pickle file
    res.to_pickle('extraction_results')
    extraction_time = time.perf_counter() - extraction_start
    
    logging.debug(f'Extraction Finished in {extraction_time} s')