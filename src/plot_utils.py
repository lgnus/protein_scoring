"""
Collection of useful functions to create plots and graphics 
"""

import sys
from collections import Counter

import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.metrics import roc_curve, auc, precision_recall_curve, confusion_matrix, average_precision_score

import src.model_utils as mu


def plot_roc_curve(models, y, legend=False, figsize=(10, 5)):
    """ Plots ROC curve for all the models specified
    
    Parameters
    ----------
    models : dict{model_name, y_probs[n_samples]}
        Map of (model_name: y_probs) with y_probs being probability of the classifier that 
        a sample belongs to the positive class
    y : array-like, shape = [n_samples]
        True labels for the provided dataset
    legend : bool
        Whether or not to display the legend of the models
    figsize : 2-tuple(int)
        Size of the plot
    """
    # obtain the false positive and true positive rates for each model
    for model_name, y_probs in models.items():
        fpr, tpr, _ = roc_curve(y.native, y_probs)
        plt.plot(fpr, tpr, label=f'{model_name} :AUC = {auc(fpr, tpr):.4f}')
    
    # plot settings
    plt.plot([0, 1], ls='--', label='Random', color='gray')
    plt.xlim((-.05,1.05))
    plt.ylim((-.05,1.05))
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    if legend:
        plt.legend(loc='lower right', fancybox=True, shadow=True)


def plot_precision_recall_curve(models, y, legend=False, figsize=(10, 5)):
    """ Plots Precision-Recall curve for all the models specified
    
    Parameters
    ----------
    models : dict{model_name, y_probs[n_samples]}
        Map of (model_name: y_probs) with y_probs being probability of the classifier that 
        a sample belongs to the positive class
    y : array-like, shape = [n_samples]
        True labels for the provided dataset
    legend : bool
        Whether or not to display the legend of the models
    figsize : 2-tuple(int)
        Size of the plot
    """    
    # Get expected performance of random classifier
    c = Counter(y.native)
    rand_prediction = c[True] / (c[True] + c[False])
    
    # obtain and plot the precision & recall per threshold for each model
    for model_name, y_probs in models.items():
        precision, recall, _ = precision_recall_curve(y.native, y_probs)
        plt.plot(recall, precision, label=f'{model_name} - AP: {average_precision_score(y.native, y_probs):.4f}')

    # plot settings
    plt.title(f'Precision-Recall Curve (AuPR)')
    plt.plot([rand_prediction, rand_prediction], ls='--', color='gray', label='Random')
    plt.xlim((-.05,1.05))
    plt.ylim((-.05,1.05))
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    if legend:
        plt.legend(loc='upper right', fancybox=True, shadow=True)


def plot_confusion_matrix(y_preds, y, normalized=True):
    """ Plots the confusion matrix.

    Parameters
    ----------
    y_preds : array-like, shape = [n_samples]
        Predicted labels of a given classifier
    y : array-like, shape = [n_samples]
        True labels for the data
    normalized : bool
        Whether or not the confusion matrix should be normalized
    """
    # get sklearn's confusion matrix and normalize it if the option is set true
    cm = confusion_matrix(y.native, y_preds)
    if normalized:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]   # from sklearn example

    cm = pd.DataFrame(cm, index=['Negative', 'Positive'], columns=['Negative', 'Positive'])
    # plot the normalized confusion matrix
    sns.heatmap(cm, annot=True, cmap='Blues')
    plt.title('Confusion Matrix')
    plt.xlabel('Predicted Class')
    plt.ylabel('True Class')


def plot_performance(y_probs, y, y_test, figsize=(20, 5)):
    """ Plots several performance accessment graphics, namely Precision-Recall
    curve, ROC curve and confusion matrix.

    Parameters
    ----------
    y_probs : array-like, shape = [n_samples]
        Probabilities of a given classifier that a sample belongs to the positive class
    y : array-like, shape = [n_samples]
        Predicted labels of a given classifier
    y_test : array-like, shape = [n_samples]
        True labels for the data
    figsize : 2-tuple(int)
        Size of the plot
    """
    plt.figure(figsize=figsize)

    models = {'Model': y_probs}

    plt.subplot(1, 3, 1)
    plot_precision_recall_curve(models, y_test)
    plt.subplot(1, 3, 2)
    plot_roc_curve(models, y_test)
    plt.subplot(1, 3, 3)
    plot_confusion_matrix(y, y_test, normalized=True)


def plot_comparative_performance(models, y, figsize=(20,5)):
    """ Plots comparative AuPR and ROC curves for each of the provided
    models

    Parameters
    ----------
        models : array-like
            array of different classification models (must provide probabilities)
        x_test: array-like
            test data to check performance
        y_test : array-like
            true labels of the test dataset
    """
    plt.figure(figsize=figsize)

    plt.subplot(1, 2, 1)
    plot_precision_recall_curve(models, y, legend=True)
    plt.subplot(1, 2, 2)
    plot_roc_curve(models, y, legend=True)
    
    
def plot_ranking_performance(models, sort_ascending=True, figsize=(15, 8),score_col='score'):
    """ Plots the cumulative success rate of native-like hits against
    the number of predictions for multiple provided models.

    Parameters
    ----------  
        models: Dict{str(model_name), array-like(score), dataframe containing labels and pdb_codes}
            dictionary of model name followed by the given score to this
            y dataframe
        sort_ascending : boolean
            True if lower score is equal to a better model, False otherwise
    """
    # prepare the plot
    plt.ylabel('Success Rate (%)')
    plt.xlabel('Number of Predictions')
    plt.xscale('log')
    plt.xlim([1, 100])
    plt.yticks(np.arange(0, 101, 10))
    plt.xticks([1, 10, 100], labels=[1, 10, 100])

    # for each model, calculate the values and plot them
    for model_name, y in models.items():

        # get the scores from each model and sort the complexes by score
        results = y[['pdb_code', 'native', score_col]].copy()
        results.sort_values(score_col, inplace=True, ascending=sort_ascending)

        # get the predicted ranks for each pdb in the set
        hits = []
        for pdb in y.pdb_code.unique():
            temp = results.loc[results.pdb_code == pdb].copy()
            temp['rank'] = list(range(1, 101))
            hits.append(int(temp.loc[temp.native == True, 'rank']))
    
        # calculate the success rate at top n predictions
        hit_count = pd.DataFrame(sorted(Counter(hits).items()), columns=['rank', 'counter'])
        hit_stats = pd.DataFrame(data=np.arange(1, 101), columns=['rank']) 
        hit_stats = hit_stats.merge(hit_count, how='outer', on='rank')
        hit_stats.fillna(0, inplace=True)

        hit_stats['cum_sum'] = np.cumsum(hit_stats['counter'])
        hit_stats['cum_perc'] = (hit_stats['cum_sum'] / np.sum(hit_stats['counter'])) * 100

        # plot the valueslabel=f'{model_name} :AUC = {auc(fpr, tpr):.4f}
        if model_name != 'Random':
            plt.plot(hit_stats['rank'], hit_stats['cum_perc'], label=f'{model_name}', linewidth=3.0)
        else:
            plt.plot(hit_stats['rank'], hit_stats['cum_perc'], label=f'{model_name}', linewidth=3.0)

    
    # add legend + display plot
    plt.legend()
    # plt.show()


def plot_ranking_groupbar(models, sort_ascending=True, figsize=(15, 8),score_col='score', kind='bar'):
    """ Plots the cumulative success rate of native-like hits against
    the number of predictions for multiple provided models.

    Parameters
    ----------  
        models: Dict{str(model_name), array-like(score), dataframe containing labels and pdb_codes}
            dictionary of model name followed by the given score to this
            y dataframe
        sort_ascending : boolean
            True if lower score is equal to a better model, False otherwise
    """
    # for each model, calculate the values and plot them
    # bar = pd.DataFrame(index=['Top 1', 'Top 10', 'Top 50'])
    bar = pd.DataFrame(columns=['Top N Predictions', ' ', 'Model'])

    for model_name, y in models.items():

        # get the scores from each model and sort the complexes by score
        results = y[['pdb_code', 'native', score_col]].copy()
        results.sort_values(score_col, inplace=True, ascending=sort_ascending)

        # get the predicted ranks for each pdb in the set
        hits = []
        for pdb in y.pdb_code.unique():
            temp = results.loc[results.pdb_code == pdb].copy()
            temp['rank'] = list(range(1, 101))
            hits.append(int(temp.loc[temp.native == True, 'rank']))
    
        # calculate the success rate at top n predictions
        hit_count = pd.DataFrame(sorted(Counter(hits).items()), columns=['rank', 'counter'])
        hit_stats = pd.DataFrame(data=np.arange(1, 101), columns=['rank']) 
        hit_stats = hit_stats.merge(hit_count, how='outer', on='rank')
        hit_stats.fillna(0, inplace=True)

        top1 = (hit_stats.counter.iloc[0] / sum(hit_stats.counter)) *100
        top10 = (sum(hit_stats.counter.iloc[:10]) / sum(hit_stats.counter)) *100
        top50 = (sum(hit_stats.counter.iloc[:50]) / sum(hit_stats.counter)) *100

        bar = bar.append(dict(zip(bar.columns, ['Top 1', top1, model_name])), ignore_index=True)
        bar = bar.append(dict(zip(bar.columns, ['Top 10', top10, model_name])), ignore_index=True)
        bar = bar.append(dict(zip(bar.columns, ['Top 50', top50, model_name])), ignore_index=True)
        # plot the values
    #    bar[model_name] = [top1, top10, top50]

    # add legend + display plot
    #ax = bar.plot(kind=kind, width=0.8) # figsize=figsize
    ax = sns.barplot(x='Top N Predictions', y=' ', hue='Model', data=bar)
    for p in ax.patches:
        ax.annotate(str(np.round(p.get_height(), decimals=1)), 
                    (p.get_x() + p.get_width() / 2, p.get_height() + 2),
                    ha='center', va='center', rotation=0,   fontsize=8)


def plot_hit_boxplot(models, figsize=(14, 6)):
    """
    Plots a boxplot for the rank of sussessful hits for all models in <models>

    Parameters:
    -----------
        models : Dict<Name, Model>
            Dictionary containing the models and their associated names.
    """
    hit_stats = mu.get_hit_stats(models)

    plt.figure(figsize=figsize)
    plt.ylabel('Hit Rank')
    plt.xlabel('Score Functions')
    ticks = list(range(0, 101, 10))
    ticks[0] = 1
    plt.yticks(ticks, rotation=30)
    sns.boxplot(hit_stats)
    # hit_stats.boxplot()
    sns.despine()


def plot_hit_stats_heatmap(models, figsize=(14, 6)):
    """
    Plots a heatmap for the rank of sussessful hits for all models in <models>

    Parameters:
    -----------
        models : Dict<Name, Model>
            Dictionary containing the models and their associated names.
    """
    hit_stats = mu.get_hit_stats(models)

    df = hit_stats.describe().T[['mean','50%','std']].T
    df.index = ['Mean', 'Median', 'STD']
    df_n = df.T / df.T.sum()
    plt.figure(figsize=figsize)
    sns.heatmap(df_n, cmap='YlGn_r', fmt='.2f', annot=df.T, vmin=0.1, vmax=0.3, cbar=False, square=True)
    plt.yticks(rotation=30)